﻿using System;
using System.Numerics;
using Contracts.Contracts.SimplePaymentChannel;
using Nethereum.ABI;
using Nethereum.Signer;
using System.Collections.Generic;

namespace StateTest
{
    public class SenderUtil
    {
        public static byte[] constructPaymentMessage(string contractAddress, uint amount, EthECKey key)
        {
            var aBIEncode = new ABIEncode();

            byte[] abistream= aBIEncode.GetABIEncodedPacked(new ABIValue(ABIType.CreateABIType("address"), contractAddress), new ABIValue(ABIType.CreateABIType("uint256"), amount));          
            EthereumMessageSigner eths = new EthereumMessageSigner();
            
            //return key.Sign(eths.HashPrefixedMessage(abistream)).To64ByteArray();
            //return eths.
           return eths.HashAndHashPrefixedMessage(abistream);
            //return abistream;


            //string sigString= eths.HashAndSign(abistream, key);//eths.HashAndHashPrefixedMessage(abistream);
            //return Encoding.ASCII.GetBytes(sigString);


        }

        public static byte[] signMessage(byte[] msg, string privatekey, out string sigString)
        {
            var key = new EthECKey(privatekey); 
            var sig = key.SignAndCalculateV(msg);
            List<byte> completeSign = new List<byte>();
            completeSign.AddRange(sig.R);
            completeSign.AddRange(sig.S);
            completeSign.AddRange(sig.V);
            sigString = sig.ToString();
            return completeSign.ToArray();
        }

        public static byte[] signPayment( string contractAddress, uint amount, string privateKey, SimplePaymentChannelService srvc, out string sigstring)
        {
                        
           
            byte[] msg = constructPaymentMessage(contractAddress, amount,new EthECKey(privateKey));
            var t = srvc.GetAbiEncodedHashQueryAsync(new BigInteger(amount));
            t.Wait();
            if (!CheckByteArrays(msg, t.Result))
                throw new Exception("Erro msg hashes are incorrect");           
            return signMessage(msg, privateKey,out sigstring);


        }

        public static bool CheckByteArrays(byte[] arr1, byte[] arr2)
        {
            if (arr1.Length != arr2.Length)
                return false;
            for(int i=0;i<arr1.Length;i++)
            {
                if (arr1[i] != arr2[i])
                    return false;
            }
            return true;
        }
    
        /*function constructPaymentMessage(contractAddress, amount) {
         *     return abi.soliditySHA3(
        ["address", "uint256"],
        [contractAddress, amount]
    );
}

function signMessage(message, callback) {
    web3.eth.personal.sign(
        "0x" + message.toString("hex"),
        web3.eth.defaultAccount,
        callback
    );
}

// contractAddress is used to prevent cross-contract replay attacks.
// amount, in wei, specifies how much Ether should be sent.

function signPayment(contractAddress, amount, callback) {
    var message = constructPaymentMessage(contractAddress, amount);
    signMessage(message, callback);
}*/
    }
}