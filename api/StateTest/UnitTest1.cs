using Nethereum.Hex.HexTypes;
using Nethereum.RPC.Eth.DTOs;
using Nethereum.Web3;
using Nethereum.Web3.Accounts;
using NUnit.Framework;
using System;
using System.Numerics;
using System.Threading;
using System.Threading.Tasks;
using Contracts.Contracts.SimplePaymentChannel;
using Nethereum.Util;
using Nethereum.HdWallet;
using Nethereum.Hex.HexTypes;
using Nethereum.RPC.Eth.DTOs;
using Nethereum.Web3;
using Nethereum.Web3.Accounts;
using static System.Net.Mime.MediaTypeNames;

namespace StateTest
{
    //Samle converted from
    // https://solidity.readthedocs.io/en/develop/solidity-by-example.html#micropayment-channel
    // fallback needed to be added to contract to transfer Eth, and so contract was modified
    //
    //about signatures: https://yos.io/2018/11/16/ethereum-signatures/
    public class Tests
    {
        string URL = "http://127.0.0.1:7545";
        BigInteger initAmount = new BigInteger(1000000);

        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void Test1()
        {
            string defaultPrivateKey = "0x72a84ccd527450bc17aa60af95a0ecde3cb1abcdf15a382a3e2d11743cfc1cbf";
            string alicePk = "0x5415418BA8573BA78C015311E11203EF91DE0B5D7D5BDDD0A3AE385D4A6AC1CC";
            string bobPk= "0x5415418BA8573BA78C015311E11203EF91DE0B5D7D5BDDD0A3AE385D4A6AC1CD";
           
            
            Account def = new Account(defaultPrivateKey);
            //fund alice and bob to start off with
            Account aliceAcc = new Account(alicePk);
            Account bobAcc = new Account(bobPk);
            Task t = Fund(def, aliceAcc, bobAcc, initAmount);
            t.Wait();

            Web3 bobConn = new Web3(bobAcc, URL);
            Web3 aliceConn = new Web3(aliceAcc, URL);
            Web3 defconn = new Web3(def, URL);
            //1. Open state chanel by creating a contract whree Bob is the owner
            var t2 = SimplePaymentChannelService.DeployContractAndWaitForReceiptAsync(bobConn, new Contracts.Contracts.SimplePaymentChannel.ContractDefinition.SimplePaymentChannelDeployment
            {
                Recipient = aliceAcc.Address,
                Duration=100000000000
            });
            t2.Wait();
            bobConn.Eth.TransactionManager.DefaultGas = new HexBigInteger(36348121); 
            aliceConn.Eth.TransactionManager.DefaultGas = new HexBigInteger(36348121);
            //2. Bob puts payment amount in contract
            Send(bobConn,bobAcc.Address, t2.Result.ContractAddress, new BigInteger(10));
            //3. now Bob send signed msg to Alice
            SimplePaymentChannelService spc = new SimplePaymentChannelService(aliceConn, t2.Result.ContractAddress);
            string sigString;
            byte[] signedMsgToAlice = SenderUtil.signPayment( t2.Result.ContractAddress, 10, bobPk,spc, out sigString);
            //4. Alice verifies that signature, amount are as expected, currently not working
            //TODO: fix this
            //Assert.IsTrue(ReciverUtil.isValidSignature(t2.Result.ContractAddress, 10,sigString, bobAcc.Address), "Bobs signature failed");
            //5. Alice closes contract and gets funds by closing contract.
            //   The contract gets destroyed and remaining eth gets returned to Bob
            var t3= spc.CloseRequestAndWaitForReceiptAsync(new Contracts.Contracts.SimplePaymentChannel.ContractDefinition.CloseFunction
            {
                Amount = 10,
                Signature =signedMsgToAlice
            });
            t3.Wait(); 
        }


        public void Send(Web3 connection, string from, string to , BigInteger ethNum)
        {
            var ethAmountWEI = (BigInteger)Nethereum.Util.UnitConversion.Convert.ToWei(ethNum);
            var t1 = connection.Eth.TransactionManager.SendTransactionAsync(from ,to, new HexBigInteger(ethAmountWEI));
            t1.Wait();
            var t2 = MineAndGetReceiptAsync(connection, t1.Result);
            t2.Wait(); 
            // web3.eth.sendTransaction
            CancellationTokenSource tkn = new CancellationTokenSource();
           /* var t = connection.Eth.TransactionManager.SendTransactionAndWaitForReceiptAsync(new TransactionInput
            {
                From = from,
                To = to,
                Value = new HexBigInteger(ethNum)                
            },tkn);
            t.Wait();*/
             var balance =  connection.Eth.GetBalance.SendRequestAsync(to);
            balance.Wait();
            var bal = balance.Result;
        }

        public  async Task Fund(Account defaultAct, Account act, Account act2,  BigInteger ethNum )
        { //todo make this take parameter list of accounts

            Web3 eth = new Web3(defaultAct, URL);
            
                var ethAmountWEI = (BigInteger)Nethereum.Util.UnitConversion.Convert.ToWei(ethNum);
                var t1 = eth.Eth.TransactionManager.SendTransactionAsync(defaultAct.Address, act.Address, new HexBigInteger(ethAmountWEI));
                t1.Wait();
                var t2 = MineAndGetReceiptAsync(eth, t1.Result);
                t2.Wait();
                //await eth.Eth.TransactionManager.SendTransactionAndWaitForReceiptAsync(new TransactionInput(TestValues.defaultAccount.Address, act.Address, new HexBigInteger(amountWEI)), new CancellationTokenSource());
                 
                t1 = eth.Eth.TransactionManager.SendTransactionAsync(defaultAct.Address, act2.Address, new HexBigInteger(ethAmountWEI));
                t1.Wait();
                t2 = MineAndGetReceiptAsync(eth, t1.Result);
                t2.Wait();        
            var balance = eth.Eth.GetBalance.SendRequestAsync(act.Address);
            balance.Wait();
            var bal = balance.Result;



        }


        public  async Task<TransactionReceipt> MineAndGetReceiptAsync(Web3 web3, string transactionHash, string errmsg = "")
        {

            var receipt = await web3.Eth.Transactions.GetTransactionReceipt.SendRequestAsync(transactionHash);

            while (receipt == null)
            {
                Thread.Sleep(1000);
                receipt = await web3.Eth.Transactions.GetTransactionReceipt.SendRequestAsync(transactionHash);
            }
            if (receipt.HasErrors() == true)
                throw new Exception("Error transaction has failed at" + errmsg);
            return receipt;
        }
    }
}