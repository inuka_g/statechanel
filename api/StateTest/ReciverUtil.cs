﻿using Nethereum.ABI;
using System.Text;
using Nethereum.Signer;

namespace StateTest
{
    public class ReciverUtil
    {
        public static byte[] prefixed(string contractAddress, uint amount)
        {
            var aBIEncode = new ABIEncode();

            byte[] abistream = aBIEncode.GetABIEncodedPacked(new ABIValue(ABIType.CreateABIType("address"), contractAddress), new ABIValue(ABIType.CreateABIType("uint256"), amount));
            EthereumMessageSigner eths = new EthereumMessageSigner();
            //return key.Sign(eths.HashPrefixedMessage(abistream)).To64ByteArray();
            //return eths.
            return eths.HashAndHashPrefixedMessage(abistream);
            //return abistream;


            //string sigString= eths.HashAndSign(abistream, key);//eths.HashAndHashPrefixedMessage(abistream);
            //return Encoding.ASCII.GetBytes(sigString);


        }

        public static string recoverSigner(byte[] msg, string signature)
        {
          
            EthereumMessageSigner eths = new EthereumMessageSigner();
            return eths.EcRecover(msg, signature );
            


        }

        public static bool isValidSignature(string contractAddress, uint amount, string signature, string expectedSigner)
        {
            byte[] msg = prefixed(contractAddress, amount);
            var signer = recoverSigner(msg, signature);
            return expectedSigner.ToLower() ==
                signer.ToLower();
        }
        /*
         * // this mimics the prefixing behavior of the eth_sign JSON-RPC method.
    function prefixed(hash) {
        return ethereumjs.ABI.soliditySHA3(
            ["string", "bytes32"],
            ["\x19Ethereum Signed Message:\n32", hash]
        );
    }

    function recoverSigner(message, signature) {
        var split = ethereumjs.Util.fromRpcSig(signature);
        var publicKey = ethereumjs.Util.ecrecover(message, split.v, split.r, split.s);
        var signer = ethereumjs.Util.pubToAddress(publicKey).toString("hex");
        return signer;
    }

    function isValidSignature(contractAddress, amount, signature, expectedSigner) {
        var message = prefixed(constructPaymentMessage(contractAddress, amount));
        var signer = recoverSigner(message, signature);
        return signer.toLowerCase() ==
            ethereumjs.Util.stripHexPrefix(expectedSigner).toLowerCase();
    }*/
    }
}