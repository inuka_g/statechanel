using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Numerics;
using Nethereum.Hex.HexTypes;
using Nethereum.ABI.FunctionEncoding.Attributes;
using Nethereum.Web3;
using Nethereum.RPC.Eth.DTOs;
using Nethereum.Contracts.CQS;
using Nethereum.Contracts.ContractHandlers;
using Nethereum.Contracts;
using System.Threading;
using Contracts.Contracts.SimplePaymentChannel.ContractDefinition;

namespace Contracts.Contracts.SimplePaymentChannel
{
    public partial class SimplePaymentChannelService
    {
        public static Task<TransactionReceipt> DeployContractAndWaitForReceiptAsync(Nethereum.Web3.Web3 web3, SimplePaymentChannelDeployment simplePaymentChannelDeployment, CancellationTokenSource cancellationTokenSource = null)
        {
            return web3.Eth.GetContractDeploymentHandler<SimplePaymentChannelDeployment>().SendRequestAndWaitForReceiptAsync(simplePaymentChannelDeployment, cancellationTokenSource);
        }

        public static Task<string> DeployContractAsync(Nethereum.Web3.Web3 web3, SimplePaymentChannelDeployment simplePaymentChannelDeployment)
        {
            return web3.Eth.GetContractDeploymentHandler<SimplePaymentChannelDeployment>().SendRequestAsync(simplePaymentChannelDeployment);
        }

        public static async Task<SimplePaymentChannelService> DeployContractAndGetServiceAsync(Nethereum.Web3.Web3 web3, SimplePaymentChannelDeployment simplePaymentChannelDeployment, CancellationTokenSource cancellationTokenSource = null)
        {
            var receipt = await DeployContractAndWaitForReceiptAsync(web3, simplePaymentChannelDeployment, cancellationTokenSource);
            return new SimplePaymentChannelService(web3, receipt.ContractAddress);
        }

        protected Nethereum.Web3.Web3 Web3{ get; }

        public ContractHandler ContractHandler { get; }

        public SimplePaymentChannelService(Nethereum.Web3.Web3 web3, string contractAddress)
        {
            Web3 = web3;
            ContractHandler = web3.Eth.GetContractHandler(contractAddress);
        }

        public Task<string> ClaimTimeoutRequestAsync(ClaimTimeoutFunction claimTimeoutFunction)
        {
             return ContractHandler.SendRequestAsync(claimTimeoutFunction);
        }

        public Task<string> ClaimTimeoutRequestAsync()
        {
             return ContractHandler.SendRequestAsync<ClaimTimeoutFunction>();
        }

        public Task<TransactionReceipt> ClaimTimeoutRequestAndWaitForReceiptAsync(ClaimTimeoutFunction claimTimeoutFunction, CancellationTokenSource cancellationToken = null)
        {
             return ContractHandler.SendRequestAndWaitForReceiptAsync(claimTimeoutFunction, cancellationToken);
        }

        public Task<TransactionReceipt> ClaimTimeoutRequestAndWaitForReceiptAsync(CancellationTokenSource cancellationToken = null)
        {
             return ContractHandler.SendRequestAndWaitForReceiptAsync<ClaimTimeoutFunction>(null, cancellationToken);
        }

        public Task<string> CloseRequestAsync(CloseFunction closeFunction)
        {
             return ContractHandler.SendRequestAsync(closeFunction);
        }

        public Task<TransactionReceipt> CloseRequestAndWaitForReceiptAsync(CloseFunction closeFunction, CancellationTokenSource cancellationToken = null)
        {
             return ContractHandler.SendRequestAndWaitForReceiptAsync(closeFunction, cancellationToken);
        }

        public Task<string> CloseRequestAsync(BigInteger amount, byte[] signature)
        {
            var closeFunction = new CloseFunction();
                closeFunction.Amount = amount;
                closeFunction.Signature = signature;
            
             return ContractHandler.SendRequestAsync(closeFunction);
        }

        public Task<TransactionReceipt> CloseRequestAndWaitForReceiptAsync(BigInteger amount, byte[] signature, CancellationTokenSource cancellationToken = null)
        {
            var closeFunction = new CloseFunction();
                closeFunction.Amount = amount;
                closeFunction.Signature = signature;
            
             return ContractHandler.SendRequestAndWaitForReceiptAsync(closeFunction, cancellationToken);
        }

        public Task<BigInteger> ExpirationQueryAsync(ExpirationFunction expirationFunction, BlockParameter blockParameter = null)
        {
            return ContractHandler.QueryAsync<ExpirationFunction, BigInteger>(expirationFunction, blockParameter);
        }

        
        public Task<BigInteger> ExpirationQueryAsync(BlockParameter blockParameter = null)
        {
            return ContractHandler.QueryAsync<ExpirationFunction, BigInteger>(null, blockParameter);
        }

        public Task<string> ExtendRequestAsync(ExtendFunction extendFunction)
        {
             return ContractHandler.SendRequestAsync(extendFunction);
        }

        public Task<TransactionReceipt> ExtendRequestAndWaitForReceiptAsync(ExtendFunction extendFunction, CancellationTokenSource cancellationToken = null)
        {
             return ContractHandler.SendRequestAndWaitForReceiptAsync(extendFunction, cancellationToken);
        }

        public Task<string> ExtendRequestAsync(BigInteger newExpiration)
        {
            var extendFunction = new ExtendFunction();
                extendFunction.NewExpiration = newExpiration;
            
             return ContractHandler.SendRequestAsync(extendFunction);
        }

        public Task<TransactionReceipt> ExtendRequestAndWaitForReceiptAsync(BigInteger newExpiration, CancellationTokenSource cancellationToken = null)
        {
            var extendFunction = new ExtendFunction();
                extendFunction.NewExpiration = newExpiration;
            
             return ContractHandler.SendRequestAndWaitForReceiptAsync(extendFunction, cancellationToken);
        }

        public Task<byte[]> GetAbiEncodedHashQueryAsync(GetAbiEncodedHashFunction getAbiEncodedHashFunction, BlockParameter blockParameter = null)
        {
            return ContractHandler.QueryAsync<GetAbiEncodedHashFunction, byte[]>(getAbiEncodedHashFunction, blockParameter);
        }

        
        public Task<byte[]> GetAbiEncodedHashQueryAsync(BigInteger amount, BlockParameter blockParameter = null)
        {
            var getAbiEncodedHashFunction = new GetAbiEncodedHashFunction();
                getAbiEncodedHashFunction.Amount = amount;
            
            return ContractHandler.QueryAsync<GetAbiEncodedHashFunction, byte[]>(getAbiEncodedHashFunction, blockParameter);
        }

        public Task<bool> IsValidSignatureQueryAsync(IsValidSignatureFunction isValidSignatureFunction, BlockParameter blockParameter = null)
        {
            return ContractHandler.QueryAsync<IsValidSignatureFunction, bool>(isValidSignatureFunction, blockParameter);
        }

        
        public Task<bool> IsValidSignatureQueryAsync(BigInteger amount, byte[] signature, BlockParameter blockParameter = null)
        {
            var isValidSignatureFunction = new IsValidSignatureFunction();
                isValidSignatureFunction.Amount = amount;
                isValidSignatureFunction.Signature = signature;
            
            return ContractHandler.QueryAsync<IsValidSignatureFunction, bool>(isValidSignatureFunction, blockParameter);
        }

        public Task<string> RecipientQueryAsync(RecipientFunction recipientFunction, BlockParameter blockParameter = null)
        {
            return ContractHandler.QueryAsync<RecipientFunction, string>(recipientFunction, blockParameter);
        }

        
        public Task<string> RecipientQueryAsync(BlockParameter blockParameter = null)
        {
            return ContractHandler.QueryAsync<RecipientFunction, string>(null, blockParameter);
        }

        public Task<string> SenderQueryAsync(SenderFunction senderFunction, BlockParameter blockParameter = null)
        {
            return ContractHandler.QueryAsync<SenderFunction, string>(senderFunction, blockParameter);
        }

        
        public Task<string> SenderQueryAsync(BlockParameter blockParameter = null)
        {
            return ContractHandler.QueryAsync<SenderFunction, string>(null, blockParameter);
        }

        public Task<SplitSignatureOutputDTO> SplitSignatureQueryAsync(SplitSignatureFunction splitSignatureFunction, BlockParameter blockParameter = null)
        {
            return ContractHandler.QueryDeserializingToObjectAsync<SplitSignatureFunction, SplitSignatureOutputDTO>(splitSignatureFunction, blockParameter);
        }

        public Task<SplitSignatureOutputDTO> SplitSignatureQueryAsync(byte[] sig, BlockParameter blockParameter = null)
        {
            var splitSignatureFunction = new SplitSignatureFunction();
                splitSignatureFunction.Sig = sig;
            
            return ContractHandler.QueryDeserializingToObjectAsync<SplitSignatureFunction, SplitSignatureOutputDTO>(splitSignatureFunction, blockParameter);
        }
    }
}
