using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Numerics;
using Nethereum.Hex.HexTypes;
using Nethereum.ABI.FunctionEncoding.Attributes;
using Nethereum.Web3;
using Nethereum.RPC.Eth.DTOs;
using Nethereum.Contracts.CQS;
using Nethereum.Contracts;
using System.Threading;

namespace Contracts.Contracts.SimplePaymentChannel.ContractDefinition
{


    public partial class SimplePaymentChannelDeployment : SimplePaymentChannelDeploymentBase
    {
        public SimplePaymentChannelDeployment() : base(BYTECODE) { }
        public SimplePaymentChannelDeployment(string byteCode) : base(byteCode) { }
    }

    public class SimplePaymentChannelDeploymentBase : ContractDeploymentMessage
    {
        public static string BYTECODE = "60806040526040516108163803806108168339818101604052604081101561002657600080fd5b508051602090910151600080546001600160a01b03199081163317909155600180546001600160a01b03909416939091169290921790915542016002556107a4806100726000396000f3fe60806040526004361061008a5760003560e01c806366d003ac1161005957806366d003ac146101b357806367e404ce146101e45780639714378c146101f9578063a7bb580314610223578063b2af9362146102f857610091565b80630e1da6c31461009357806316c675fb146100a8578063415ffba7146100e45780634665096d1461019e57610091565b3661009157005b005b34801561009f57600080fd5b506100916103c6565b3480156100b457600080fd5b506100d2600480360360208110156100cb57600080fd5b503561041a565b60408051918252519081900360200190f35b3480156100f057600080fd5b506100916004803603604081101561010757600080fd5b8135919081019060408101602082013564010000000081111561012957600080fd5b82018360208201111561013b57600080fd5b8035906020019184600183028401116401000000008311171561015d57600080fd5b91908080601f01602080910402602001604051908101604052809392919081815260200183838082843760009201919091525092955061046e945050505050565b3480156101aa57600080fd5b506100d2610557565b3480156101bf57600080fd5b506101c861055d565b604080516001600160a01b039092168252519081900360200190f35b3480156101f057600080fd5b506101c861056c565b34801561020557600080fd5b506100916004803603602081101561021c57600080fd5b503561057b565b34801561022f57600080fd5b506102d66004803603602081101561024657600080fd5b81019060208101813564010000000081111561026157600080fd5b82018360208201111561027357600080fd5b8035906020019184600183028401116401000000008311171561029557600080fd5b91908080601f0160208091040260200160405190810160405280939291908181526020018383808284376000920191909152509295506105a5945050505050565b6040805160ff9094168452602084019290925282820152519081900360600190f35b34801561030457600080fd5b506103b26004803603604081101561031b57600080fd5b8135919081019060408101602082013564010000000081111561033d57600080fd5b82018360208201111561034f57600080fd5b8035906020019184600183028401116401000000008311171561037157600080fd5b91908080601f01602080910402602001604051908101604052809392919081815260200183838082843760009201919091525092955061061c945050505050565b604080519115158252519081900360200190f35b60025442101561040c576040805162461bcd60e51b815260206004820152600c60248201526b1d1a5b5948195e1c1a5c995960a21b604482015290519081900360640190fd5b6000546001600160a01b0316ff5b6000610468308360405160200180836001600160a01b03166001600160a01b031660601b81526014018281526020019250505060405160208183030381529060405280519060200120610696565b92915050565b6001546001600160a01b031633146104bd576040805162461bcd60e51b815260206004820152600d60248201526c1b9bdd081c9958da5c1a595b9d609a1b604482015290519081900360640190fd5b6104c7828261061c565b61050e576040805162461bcd60e51b81526020600482015260136024820152721cda59db985d1d5c99481b9bdd081d985b1a59606a1b604482015290519081900360640190fd5b6001546040516001600160a01b039091169083156108fc029084906000818181858888f19350505050158015610548573d6000803e3d6000fd5b506000546001600160a01b0316ff5b60025481565b6001546001600160a01b031681565b6000546001600160a01b031681565b6000546001600160a01b0316331461059257600080fd5b60025481116105a057600080fd5b600255565b60008060008351604114610600576040805162461bcd60e51b815260206004820152601760248201527f7369676e6174757265206c656e677468206e6f74203635000000000000000000604482015290519081900360640190fd5b5050506020810151604082015160609092015160001a92909190565b60008061066b308560405160200180836001600160a01b03166001600160a01b031660601b81526014018281526020019250505060405160208183030381529060405280519060200120610696565b6000549091506001600160a01b031661068482856106e7565b6001600160a01b031614949350505050565b604080517f19457468657265756d205369676e6564204d6573736167653a0a333200000000602080830191909152603c8083019490945282518083039094018452605c909101909152815191012090565b6000806000806106f6856105a5565b92509250925060018684848460405160008152602001604052604051808581526020018460ff1660ff1681526020018381526020018281526020019450505050506020604051602081039080840390855afa158015610759573d6000803e3d6000fd5b5050604051601f19015197965050505050505056fea26469706673582212204b90fbe9162d680bfac1c1e5e2061b959e4e89cf74886367303c1bf5d38d50f264736f6c63430006010033";
        public SimplePaymentChannelDeploymentBase() : base(BYTECODE) { }
        public SimplePaymentChannelDeploymentBase(string byteCode) : base(byteCode) { }
        [Parameter("address", "_recipient", 1)]
        public virtual string Recipient { get; set; }
        [Parameter("uint256", "duration", 2)]
        public virtual BigInteger Duration { get; set; }
    }

    public partial class ClaimTimeoutFunction : ClaimTimeoutFunctionBase { }

    [Function("claimTimeout")]
    public class ClaimTimeoutFunctionBase : FunctionMessage
    {

    }

    public partial class CloseFunction : CloseFunctionBase { }

    [Function("close")]
    public class CloseFunctionBase : FunctionMessage
    {
        [Parameter("uint256", "amount", 1)]
        public virtual BigInteger Amount { get; set; }
        [Parameter("bytes", "signature", 2)]
        public virtual byte[] Signature { get; set; }
    }

    public partial class ExpirationFunction : ExpirationFunctionBase { }

    [Function("expiration", "uint256")]
    public class ExpirationFunctionBase : FunctionMessage
    {

    }

    public partial class ExtendFunction : ExtendFunctionBase { }

    [Function("extend")]
    public class ExtendFunctionBase : FunctionMessage
    {
        [Parameter("uint256", "newExpiration", 1)]
        public virtual BigInteger NewExpiration { get; set; }
    }

    public partial class GetAbiEncodedHashFunction : GetAbiEncodedHashFunctionBase { }

    [Function("getAbiEncodedHash", "bytes32")]
    public class GetAbiEncodedHashFunctionBase : FunctionMessage
    {
        [Parameter("uint256", "amount", 1)]
        public virtual BigInteger Amount { get; set; }
    }

    public partial class IsValidSignatureFunction : IsValidSignatureFunctionBase { }

    [Function("isValidSignature", "bool")]
    public class IsValidSignatureFunctionBase : FunctionMessage
    {
        [Parameter("uint256", "amount", 1)]
        public virtual BigInteger Amount { get; set; }
        [Parameter("bytes", "signature", 2)]
        public virtual byte[] Signature { get; set; }
    }

    public partial class RecipientFunction : RecipientFunctionBase { }

    [Function("recipient", "address")]
    public class RecipientFunctionBase : FunctionMessage
    {

    }

    public partial class SenderFunction : SenderFunctionBase { }

    [Function("sender", "address")]
    public class SenderFunctionBase : FunctionMessage
    {

    }

    public partial class SplitSignatureFunction : SplitSignatureFunctionBase { }

    [Function("splitSignature", typeof(SplitSignatureOutputDTO))]
    public class SplitSignatureFunctionBase : FunctionMessage
    {
        [Parameter("bytes", "sig", 1)]
        public virtual byte[] Sig { get; set; }
    }





    public partial class ExpirationOutputDTO : ExpirationOutputDTOBase { }

    [FunctionOutput]
    public class ExpirationOutputDTOBase : IFunctionOutputDTO 
    {
        [Parameter("uint256", "", 1)]
        public virtual BigInteger ReturnValue1 { get; set; }
    }



    public partial class GetAbiEncodedHashOutputDTO : GetAbiEncodedHashOutputDTOBase { }

    [FunctionOutput]
    public class GetAbiEncodedHashOutputDTOBase : IFunctionOutputDTO 
    {
        [Parameter("bytes32", "", 1)]
        public virtual byte[] ReturnValue1 { get; set; }
    }

    public partial class IsValidSignatureOutputDTO : IsValidSignatureOutputDTOBase { }

    [FunctionOutput]
    public class IsValidSignatureOutputDTOBase : IFunctionOutputDTO 
    {
        [Parameter("bool", "", 1)]
        public virtual bool ReturnValue1 { get; set; }
    }

    public partial class RecipientOutputDTO : RecipientOutputDTOBase { }

    [FunctionOutput]
    public class RecipientOutputDTOBase : IFunctionOutputDTO 
    {
        [Parameter("address", "", 1)]
        public virtual string ReturnValue1 { get; set; }
    }

    public partial class SenderOutputDTO : SenderOutputDTOBase { }

    [FunctionOutput]
    public class SenderOutputDTOBase : IFunctionOutputDTO 
    {
        [Parameter("address", "", 1)]
        public virtual string ReturnValue1 { get; set; }
    }

    public partial class SplitSignatureOutputDTO : SplitSignatureOutputDTOBase { }

    [FunctionOutput]
    public class SplitSignatureOutputDTOBase : IFunctionOutputDTO 
    {
        [Parameter("uint8", "v", 1)]
        public virtual byte V { get; set; }
        [Parameter("bytes32", "r", 2)]
        public virtual byte[] R { get; set; }
        [Parameter("bytes32", "s", 3)]
        public virtual byte[] S { get; set; }
    }
}
