
To run:
Start Ganache.
simply load solution and run test, no need to deploy as c# contract wrapper is deployed from C#


How it works:

Premise,  Bob needs to pay some amount to Alice. Payment involves a large amount of micro payments. To save Gas Bob creates a contract and puts some Eth  in it for Alice. Bob then send the micro payments as signed signatures(includes the amount and address of contract om signature).  Once Alice gets the final signature, she closes the contract and remaining Eth gets returned to Bob.

 

Bob Creates payment contract.
Bob transfers Eth to contract
Bob creates msg hash for each payment(assume the subsequent payment accounts for previous balance). Bob signs this hash and send it to Alice.
After the final payment Alice asks contract to close. She provides last signed hash signature and amount
Contract, creates a new hash using address of current contract and amount  which it then uses to recover the address of Bob(ecrecover). If the new hash is incorrect(amount is wrong or address is wrong) the recovered address is incorrect. Contract verifies the address in signature is Bob.
Contract transfers amount to Alice and kills itself and transfers remaining Eth to Bob.